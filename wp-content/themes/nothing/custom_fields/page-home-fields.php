<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action( 'carbon_fields_register_fields', 'page_home_fields_options' );
function page_home_fields_options() {
    Container::make( 'post_meta', 'Home Meta' )
        ->where( 'post_id', '=', get_option('page_on_front'))
        ->add_fields(array(
            Field::make( 'image', 'hero', __('Hero') )
        ));

}
<?php
//Hooks to get article by slug.
add_action('rest_api_init', function(){
    register_rest_route('headless-blog/v1', '/post/(?P<slug>\S+)', array(
        'methods' => 'GET',
        'callback' => 'rest_get_post_content_by_slug',
    ));
});

function rest_get_post_content_by_slug($data) {
    $query = get_posts([
        'post_type' => 'post',
        'name' => $data['slug'],
    ]);

    if(empty($query)){
        return new WP_Error( 'no_such_post', 'No such post', array( 'status' => 404 ) );
    }

    $data = array(
        'ID' => $query[0]->ID,
        'title' => apply_filters('the_title', $query[0]->post_title),
        'slug' => $query[0]->post_name,
        'content' => apply_filters('the_content', $query[0]->post_content),
        'date' => get_the_date('c', $query[0]->ID),
        'date_gmt' => $query[0]->post_date_gmt,
        'comment_open' => comments_open($query[0]->ID),
    );

    return $data;

}
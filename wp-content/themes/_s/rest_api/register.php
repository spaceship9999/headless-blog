<?php
//Hooks to register.
add_action('rest_api_init', function(){
    register_rest_route('headless-blog/v1', '/register', array(
        'methods' => 'POST',
        'callback' => 'rest_register',
    ));
});

function rest_register($request) {
    $username = $request['username'];
    $password = $request['password'];
    $repassword = $request['repassword'];
    $email = $request['email'];

    $error_fields = [];

    if(empty($username)){
        $error_fields['username'] = 'Username cannot be empty';
    }
    if(empty($password)){
        $error_fields['password'] = 'Password cannot be empty';
    }
    if(empty($repassword)){
        $error_fields['repassword'] = 'Please type your password again to verify.';
    }
    if(empty($email)) {
        $error_fields['email'] = 'Email cannot be empty';
    }
    if(!empty($error_fields)){
        $data = array(
            'success' => false,
            'message' => $error_fields,
        );
    }
    else if(!strcmp($password, $repassword)){
        $data = array(
            'success' => false,
            'message' => 'Your password inputs doesn\'t match.',
        );
    }else{
        $user = wp_create_user($username, $password, $email);
        if(!is_wp_error($user)) {
            $data = array(
                'success' => true,
                'id' => $user,
                'message' => 'The account has been registered and pending approval from the site owner.',
            );
        }else{
            $data = array(
                'success' => false,
                'message' => $user->get_error_message(),
            );
        }
    }


    return $data;
}
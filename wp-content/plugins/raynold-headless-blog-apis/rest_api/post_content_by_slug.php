<?php
//Hooks to get article by slug.
add_action('rest_api_init', function(){
    register_rest_route('headless-blog/v1', '/post/(?P<slug>\S+)', array(
        'methods' => 'GET',
        'callback' => 'rest_get_post_content_by_slug',
    ));
});

function rest_get_post_content_by_slug($data) {
    $wp_query = new WP_Query(['post_type' => 'post', 'name' => $data['slug']]);

    $query = get_posts([
        'post_type' => 'post',
        'name' => $data['slug'],
    ]);

    if(!$wp_query->have_posts()){
        return new WP_Error( 'no_such_post', 'No such post', array( 'status' => 404 ) );
    }

    $rendering = new RaynoldHeadlessRendering();

    return $rendering->post_details_filter($wp_query);

}
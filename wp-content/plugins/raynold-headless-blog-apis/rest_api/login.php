<?php

//Hooks to login
add_action('rest_api_init', function () {
    register_rest_route('headless-blog/v1', '/login/', array(
        'methods' => 'POST',
        'callback' => 'api_login',
    ));
});

function api_login() {
    $_POST = json_decode(file_get_contents('php://input'),true);
    $username = !empty($_POST['username']) ? wp_unslash($_POST['username']) : '';
    $password  = !empty($_POST['password']) ? $_POST['password'] : '';
    $remote_ip = $_SERVER['REMOTE_ADDR'];

    //Get number of trials currently stored.
    $num_trials = get_transient('login_attempts_' . $remote_ip) ?
        get_transient('login_attempts_' . $remote_ip)['trials']: 0;


    //Store username/email or password error
    $data['error'] = array();

    if(empty($username) || empty($password)){
        $data['error'] = __('Username or password cannot be empty. ');
    }

    if(!empty($username)){
        //If it's an email and can be found from the db, we will replace it will the username
        if(is_email($username)){
            if(!$username = get_user_by('email', $username)){
                $data['error'] = __('Incorrect email or password. ');
            }
        }
        else{
            if(!username_exists($username)){
                $data['error'] = __('Incorrect username or password. ');
            }
        }
        $user_auth = wp_authenticate($username, $password);
        if(is_wp_error($user_auth)){
            $data['error'] = __('Incorrect username, email or password. ');
        }
        else {
            //Check if it is pending verification. Block logging in if it is in unapproved list
            $user = get_user_by('login', $username);
            $user_roles = $user->roles;
            if (in_array('unapproved', $user_roles, true)) {
                $data['error'] = __('This user hasn\'t been approved. Please try again later.');
                //It doesn't count a trial in this scenario
                return new WP_REST_Response($data, 401);
            }
        }

    }

    /* Stop progressing when there's already an error. It acts as an additional layer to protect the
    /* Wordpress core from being exploited.
     */

    $rendering = new RaynoldHeadlessRendering();
    if(empty($data['error'])) unset($data);
    else{
        $data = $rendering->enforce_error_roadblock($data);
        return new WP_REST_Response($data, 401);
    }

    // The Record of trials isn't exists or it is lapsed, or it is less than 3 trials
    $user = wp_signon(array(
        'user_login' => $username,
        'user_password' => $password
    ), true);

    if(is_wp_error($user)) {
        $data['error'] = $user;
    }
    else{
        if (!get_transient('login_attempts_' . $remote_ip) || get_transient('login_attempts_' . $remote_ip) <= 3){
            $data['success'] = array(
                'message' => 'Logged in successfully',
                'user' => array(
                    'id' => $user->ID,
                    'username' => $user->user_login,
                    'email' => $user->user_email,
                    'roles' => $user->roles,
                    'root' => esc_url_raw(rest_url(). '/headless-blog/v1/'),
                    'nonce' => wp_create_nonce('wp_rest')
                )
            );
            delete_transient('login_attempts_' . $remote_ip);
        }
    }
    return $data;
}
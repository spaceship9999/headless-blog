<?php
//Register API to get posts by category slug
add_action('rest_api_init', function () {
    register_rest_route('headless-blog/v1', '/category/(?P<slug>\S+)', array(
        'methods' => 'GET',
        'callback' => 'rest_details_in_categories',
    ));
});

function rest_details_in_categories($slug_data)
{
    $data = array();
    $category_term = get_terms(
        ['taxonomy' => 'category', 'hide_empty' => true]
    );
    //Check if category exists
    $data['has_category'] = false;
    foreach($category_term as $category){
        if($category->slug === $slug_data['slug']){
            $data['has_category'] = true;
        }
    }
    if(!$data['has_category']) return $data;
    //Get all posts from this category
    $category_query = new WP_Query(['post_type' => 'post', 'tax_query' => [
        [
        'taxonomy' => 'category',
        'field' => 'slug',
        'terms' => $slug_data['slug'],
        'include_children' => true,
        ]
    ]
    ]);

    $rendering = new RaynoldHeadlessRendering();

    $render_data = array(
        'data' => $rendering->post_filter($category_query),
        'name' => get_term_by('slug', $slug_data['slug'], 'category')->name,
        'slug' => $slug_data['slug'],
    );
    return $render_data;
}
<?php

//Hooks to get article by slug.
add_action('rest_api_init', function(){
    register_rest_route('headless-blog/v1', '/post', array(
        'methods' => 'GET',
        'callback' => 'rest_get_all_posts_by_slug',

    ));
});

function rest_get_all_posts_by_slug($param) {
    $looper = carbon_get_theme_option('crb_global_posts');
    $ppp = 3;
    if($looper){
        foreach ($looper as $item){
            $ppp = $item['num_latest_post'];
        }
    }
    $page = $param['page'];
    if(empty($ppp) || empty($page)){
        return false;
    }

    $post_query = new WP_Query([
        'post_type' => 'post',
        'posts_per_page' => $ppp,
        'offset' => ($page - 1) * $ppp,
    ]);



    $post = array(
        'data' => array(),
    );

    if($post_query->have_posts()):
        while($post_query->have_posts()): $post_query->the_post();
            $post['data'][] = array(
                'title' => get_the_title(),
                'content' => apply_filters('the_content', get_the_content()),
                'excerpt' => get_the_excerpt(),
                'date' => get_the_date('c'),
                'thumbnail' => wp_get_attachment_image_url(get_post_thumbnail_id(get_the_ID()), '1920_1080'),
            );
        endwhile;
    endif;

    return $post;
}
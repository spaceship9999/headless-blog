<?php

//Hooks to get latest article
add_action('rest_api_init', function () {
    register_rest_route('headless-blog/v1', '/latest-post', array(
        'methods' => 'GET',
        'callback' => 'rest_get_latest_post',
    ));
});

function rest_get_latest_post(){
    $looper = carbon_get_theme_option('crb_global_posts');
    $ppp = 3;
    if($looper){
        foreach ($looper as $item){
            $ppp = $item['num_latest_post'];
        }
    }
    $post_query = new WP_Query([
        'post_type' => 'post',
        'posts_per_page' => $ppp,
    ]);

    $rendering = new RaynoldHeadlessRendering();
    return $rendering->post_filter($post_query);
}